import React from 'react';

import './App.css';

import { Routes, Route } from 'react-router-dom';

import Choise from './pages/Сhoice';
import Cabinets from './pages/Cabinets';
import RegisForInvestor from './pages/RegisForInvestor';
import RegisForBusiness from './pages/RegisForBusiness';
import HomePage from './pages/HomePage'; 

function App() {
  return (
    <>
      <Routes>
        <Route path='/' element={<HomePage />} /> 
        <Route path='/choise' element={<Choise />} />
        <Route path='/cabinets' element={<Cabinets />} />
        <Route path='/regisforinvestor' element={<RegisForInvestor />} />
        <Route path='/regisforbusiness' element={<RegisForBusiness />} />
      </Routes>
    </>
  );
}

export default App;

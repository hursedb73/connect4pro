import React, { useState } from 'react';
import '../styles/Cabinets.css'; 
import logo from '../assets/logo.png';
import axios from 'axios';

const Cabinets = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  
  const handleSubmit = async (event) => {
    event.preventDefault();
  
    try {
      const response = await axios.post('http://backend-prod.connect4.pro', {
        email: email,
        password: password
      });
      console.log('Ответ от бэкенда:', response.data); 
    } catch (error) {
      console.error('Ошибка при отправке данных на бэкенд:', error); 
    }
  };
  
  return (
    <>
      <header className="header">  
      <img src={logo} alt="Логотип" />
      </header>
      <div className="login-container">
        <form className="login-form" onSubmit={handleSubmit}>
        <p className="login-text">Войти в личный кабинет</p>
          <input
            type="email"
            id="email"
            placeholder="Почта"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
          <input
            type="password"
            id="password"
            placeholder="Пароль"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
          <div className="remember-me">
            <input type="checkbox" id="remember-me" />
            <label htmlFor="remember-me">Запомнить меня</label>
          </div>
          <button type="submit">Войти</button>
        </form>
        <div className="password-reset">
          <a href="/">Забыли пароль?</a>
          <div className="no-account">
            <p>Нет аккаунта? <a href="/">Зарегистрируйтесь</a></p>
          </div>
        </div>
      </div>
      <footer className="footer"> 
      </footer>
    </>
  );
};

export default Cabinets;

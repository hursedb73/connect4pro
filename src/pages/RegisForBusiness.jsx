import React, { useState } from 'react';
import '../styles/Business.css'; 
import logo from '../assets/logo.png'; 

const RegisForBusiness = () => {
  const [name, setName] = useState('');
  const [sectors, setSectors] = useState([]);
  const [investmentAmount, setInvestmentAmount] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [repeatPassword, setRepeatPassword] = useState('');
  const [receiveOffers, setReceiveOffers] = useState(false);
  
  const handleSectorChange = (e) => {
    const sectorValue = e.target.value;
    if (sectors.includes(sectorValue)) {
      setSectors(sectors.filter(sector => sector !== sectorValue));
    } else {
      setSectors([...sectors, sectorValue]);
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault(); 
  };

  return (
    <>
    
    <header className="headerInv"> 
    <img src={logo} alt="Логотип" />
    </header>
    <div className="registration-container">
      <h2 className='regis'>Регистрация для бизнеса</h2>
      <form className="registration-form" onSubmit={handleSubmit}>
      <input
          type="text"
          id="name"
          placeholder="Ваше имя"
          value={name}
          onChange={(e) => setName(e.target.value)}
          required
        />

<label>Какой сектор представляете?</label>
<div className="checkbox-container">
  <div>
    <input
      type="checkbox"
      id="agribusiness"
      value="agribusiness"
      checked={sectors.includes("agribusiness")}
      onChange={handleSectorChange}
    />
    <label htmlFor="agribusiness">Агробизнес</label>
  </div>
  <div>
    <input
      type="checkbox"
      id="it"
      value="it"
      checked={sectors.includes("it")}
      onChange={handleSectorChange}
    />
    <label htmlFor="it">IT</label>
  </div>
  <div>
    <input
      type="checkbox"
      id="education"
      value="education"
      checked={sectors.includes("education")}
      onChange={handleSectorChange}
    />
    <label htmlFor="education">Образование</label>
  </div> 
</div>
        <label htmlFor="investment-amount">Уже привлекали инвестиции</label>
        <select
          id="investment-amount"
          value={investmentAmount}
          onChange={(e) => setInvestmentAmount(e.target.value)}
          required
          > 
          <option value="500000-1000000">Еще нет, собираюсь на вашей платформе</option> 
        </select>

        <h3 className='data'>Данные для входа</h3> 
        <input
          type="email"
          id="email"
          placeholder="Ваша почта:"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
          /> 
        <input
          type="password"
          id="password"
          placeholder="Пароль:"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          required
          /> 
        <input
          type="repeat-password"
          id="repeat-password"
          placeholder='Повторите пароль:'
          value={repeatPassword}
          onChange={(e) => setRepeatPassword(e.target.value)}
          required
          />

        <div className="receive-offers">
          <input
            type="checkbox"
            id="receive-offers"
            checked={receiveOffers}
            onChange={(e) => setReceiveOffers(e.target.checked)}
            />
          <label htmlFor="receive-offers">Получать полезные материалы по привлечению инвестиций</label>
        </div>

        <button type="submit">Зарегистрироваться</button>
      </form>

      <div className="login-instead">
        <p>Уже есть аккаунт? <a href="/">Войти</a></p>
      </div>
    </div>
          
    <footer className="footerInv"> 
      </footer>
  </>
  );
};

export default RegisForBusiness;

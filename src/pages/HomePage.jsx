import React from 'react';
import { Link } from 'react-router-dom'; // Импортируем Link из react-router-dom
import '../styles/HomePage.css'; 
import logo from '../assets/logo.png';

const HomePage = () => {
  return (
    <div>
      <header className="header"> 
        <img src={logo} alt="Логотип" />
      </header>
      <div className="container">
        <div className="block">
          <h2>Для бизнеса</h2> 
          <Link to="/regisforbusiness"><button>Начать</button></Link> 
        </div>
        <div className="block">
          <h2>Для инвестиций</h2> 
          <Link to="/regisforinvestor"><button>Начать</button></Link>  
        </div>
      </div>
      <footer className="footer"> 
      </footer>
    </div>
  );
};

export default HomePage;
